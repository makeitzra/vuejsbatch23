export const FormLoginComponent = {
    props: ['value'],
    methods : {
        updateValue(value) {
           this.$emit('input', {
               username: this.$refs.name.value,
               password: this.$refs.pass.value
           })
        },
        
    },
    data () {
        return {
        }
    },
    template: `
    <div>
    <h2>Login Form</h2>
        <form @submit.prevent="$emit('login', value)">
            <input type="text" ref="name" placeholder="username" :value="value.username" @input="updateValue()">
            <input type="text" ref="pass" placeholder="password" :value="value.password" @input="updateValue()">

            <button type="submit">Login</button>
        </form>
        </div>
    ` 
}