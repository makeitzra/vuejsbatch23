export const MembersComponent = {

    data () {
        return {
            members : [
                { name : 'Alex'},
                { name : 'Daniel'},
                { name : 'Ezra'},
            ],
        
        }
    },
    template: `
        <div>
        <div class="member" v-for="member in members">
         <p>Member: {{ member.name }}</p>
        </div>
        </div>
    ` 
}