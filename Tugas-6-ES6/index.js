// Soal 1

const persegi = (a,b) => {
    return "Luas: " + a*b + "\n" + "Keliling: " + 2*(a+b);
}

var result = persegi(5,1);

console.log(result);
// Jawaban soal no 1

// Soal 2 

  const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
          console.log(firstName + " " + lastName)
            
        }
    }
  }

  newFunction("William", "Imoh").fullName()

//   Jawaban soal no 2

// Soal 3 

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  };

const {firstName, lastName, address, hobby} = newObject;

console.log(firstName, lastName, address, hobby)

// Jawaban no 3

// Soal no 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east]

//Driver Code
console.log(combined)

// Jawaban soal no 4

// Soal 5

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

const after = `Lorem ${view} dolor sit amet, consectetur adispiscing elit, ${planet}`

console.log(before);
console.log(after);

// Jawaban soal 5

