// Soal 1
// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

// Tulis code untuk memanggil function readBooks di sini
let readTime = 10000;

readBooks(readTime, books[0], (data) => {
        readBooks(data, books[1], (data) => {
            readBooks(data, books[2], (data) => {
                readBooks(data, books[3], (data))
            })
        })
    })
    // Jawaban soal 1