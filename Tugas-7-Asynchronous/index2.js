var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 

readBooksPromise(10000, books[0])
    .then((data) => {
        (readBooksPromise(data, books[1])
        .then((data) => (readBooksPromise(data, books[2]))))
    })
    .catch(error => console.log(error))