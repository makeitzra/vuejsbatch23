// Soal 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// output = saya senang belajar JAVASCRIPT

var ketiga = pertama.substring(0, 4);
var keempat = pertama.substring(11, 19); 
var kelima = ketiga.concat(keempat);
var keenam = kedua.substring(0,8);
var ketujuh = kedua.substring(8,18);
var ketujuh2 = ketujuh.toUpperCase();
var kedelapan = keenam.concat(ketujuh2);
var result = kelima.concat(kedelapan);
console.log(result);

// Jawaban soal no 1

// Soal 2 

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

// output = 24 (int)

var number1 = Number(kataPertama);
var number2 = Number(kataKedua);
var number3 = Number(kataKetiga);
var number4 = Number(kataKeempat);

console.log((number1 % 3) + (number1 * number2) + (number4 % number3)*2 + (number4 - number2) - (number1 / number2));

// Jawaban soal no 2

// Soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4,10); // do your own! 
var kataKetiga = kalimat.substr(15,4); // do your own! 
var kataKeempat = kalimat.substr(19,5); // do your own! 
var kataKelima = kalimat.substr(24,7); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// output = Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali

// Jawaban soal no 3