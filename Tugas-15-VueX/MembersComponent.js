export const MembersComponent = {
    props: ['data'],
    methods: {},

    data(){
        return {
            photoDomain: 'http://demo-api-vue.sanbercloud.com/'
        }
    },
    template: `<tr> 
    <td>{{ data.name }}</td>
          <td>{{ data.address }}</td>
          <td>{{ data.no_hp }}</td>
          <td>   <img width=100 :src="data.photo_profile ? photoDomain + data.photo_profile : 'https://dummyimage.com/16:9x1080'" alt=""></td>
          <td>         
              <button @click="$emiit('edit', data)">Edit</button>
            <button @click="$emit('remove', data.id)">Hapus</button>
            <button @click="$emit('uploadFoto', data)">Upload Foto</button>
        </td>
    </tr>`
}