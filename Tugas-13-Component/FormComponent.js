export const FormComponent = {
    props: ['value', 'buttonstatus', 'ideditmember', 'name', 'errors'],
    methods: {
        updateValue(value){
            this.$emit('input', {
                name: this.$refs.name.value,
                address: this.$refs.address.value,
                no_hp: this.$refs.no_hp.value,

            })
        }
    },
    data(){
        return{
            
        }
    },
    template: `
    <div>

        <form @submit.prevent="$emit('submitform', value)">
        <p v-if="errors">
        <b>Please fill the blanks correctly : </b>
        <ul>
            <li v-for="error in errors">{{ error }}</li>
        </ul>
     </p>
        <label>Name </label>
        <input type="text" name="name" placeholder="Your name.." ref="name" :value="value.name" @input="updateValue()">
    
        <label>Address</label>
        <input type="text" name="address"   placeholder="Your address.." ref="address" :value="value.address" @input="updateValue()">
    
        <label>Phone</label>
        <input type="text" name="no_hp"   placeholder="Your phone number ..." ref="no_hp" :value="value.no_hp" @input="updateValue()">
       
        <div v-if="buttonstatus === 'submit'">
        <input type="submit" value="Submit">
        </div>
        <div v-else-if="buttonstatus === 'edit'">
        <input type="button" value="Edit" @click="$emit('editmember', ideditmember)">
        <input type="button" value="Cancel"  @click="$emit('clearform')" style="margin-top: 10px;">
            </div>
            <div v-else-if="buttonstatus === 'upload'">
                <input type="file" name="photo" ref="photo">
        <input type="button" value="Upload" @click="$emit('submitPhoto', idUploadMember)" style="margin-top: 10px;">
        <input type="button" value="Cancel" @click="$emit('clearform')" style="margin-top: 10px;">
            </div>
        </form>

    </div>
    `
}